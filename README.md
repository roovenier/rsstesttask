## RssTestTask

[Подробное описание задания](https://gist.github.com/beshkenadze/9834122#file-tz_mobile_ios-md)

Необходимо создать rss reader. Приложение должно иметь минимум 3 встроенных rss подписки, пользователь может добавить свои подписки. Приложение должно выводить список подписок, при переходе по подписке надо показывать список постов, после перехода по посту надо показывать его заголовок и описание.