//
//  AddSubscriptionController.h
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Subscription;

@protocol AddSubscriptionControllerDelegate;

@interface AddSubscriptionController : UITableViewController

@property (weak, nonatomic) id <AddSubscriptionControllerDelegate> delegate;

@end

@protocol AddSubscriptionControllerDelegate

- (void)addedNewSubscription:(Subscription *)subscription;

@end
