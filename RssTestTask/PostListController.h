//
//  PostListController.h
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Subscription+CoreDataProperties.h"

@interface PostListController : UITableViewController

@property (strong, nonatomic) Subscription *subscription;

@end
