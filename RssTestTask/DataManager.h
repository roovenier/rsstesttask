//
//  DataManager.h
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

+ (DataManager *)instance;
- (void)saveContext;

@end
