//
//  PostDetailController.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "PostDetailController.h"

@interface PostDetailController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation PostDetailController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.post) {
        self.navigationItem.title = self.post.title;
        
        self.titleLabel.text = self.post.title;
        self.dateLabel.text = [self dateStringFromDateObject:self.post.pubDate];
        self.descriptionLabel.text = self.post.body;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers

- (NSString *)dateStringFromDateObject:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    
    return [formatter stringFromDate:date];
}

@end
