//
//  PostModel.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "PostModel.h"

@implementation PostModel

- (instancetype)initWithData:(NSDictionary *)dataDictionary {
    self = [super init];
    if (self) {
        self.title = [dataDictionary objectForKey:@"title"];
        self.category = [dataDictionary objectForKey:@"category"];
        self.body = [dataDictionary objectForKey:@"description"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZZ"];
        
        self.pubDate = [dateFormat dateFromString:[dataDictionary objectForKey:@"pubDate"]];
    }
    return self;
}

@end
