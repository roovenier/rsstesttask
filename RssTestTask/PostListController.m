//
//  PostListController.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "PostListController.h"
#import "XMLDictionary.h"
#import "PostModel.h"
#import "PostDetailController.h"

@interface PostListController ()

@property (strong, nonatomic) NSArray *postsArray;

@end

@implementation PostListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.subscription.title;
    
    NSURL *URL = [NSURL URLWithString:self.subscription.source];
    NSData *data = [[NSData alloc] initWithContentsOfURL:URL];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithXMLData:data];
    
    //self.postsArray = [[dictionary objectForKey:@"channel"] objectForKey:@"item"];
    
    NSMutableArray *postsTempArray = [NSMutableArray array];
    
    for(NSDictionary *item in [[dictionary objectForKey:@"channel"] objectForKey:@"item"]) {
        [postsTempArray addObject:[[PostModel alloc]  initWithData:item]];
    }
    
    if(postsTempArray.count > 0) {
        self.postsArray = postsTempArray;
    } else {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Empty" message:@"Sorry, nothing found" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* backButton = [UIAlertAction actionWithTitle:@"Return" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [alert addAction:backButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.postsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"PostCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    PostModel *post = [self.postsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    cell.textLabel.numberOfLines = 0;
    
    cell.textLabel.text = post.title;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PostModel *post = [self.postsArray objectAtIndex:indexPath.row];
    
    PostDetailController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PostDetailController"];
    vc.post = post;
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
