//
//  EditSubscriptionController.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 28.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "EditSubscriptionController.h"
#import "DataManager.h"

@interface EditSubscriptionController ()

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *sourceTextField;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation EditSubscriptionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.subscription.title;
    
    if(self.subscription) {
        self.titleTextField.text = self.subscription.title;
        self.sourceTextField.text = self.subscription.source;
    }
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveSubscription:)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
    self.managedObjectContext = [[[DataManager instance] persistentContainer] viewContext];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)saveSubscription:(UIBarButtonItem *)sender {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Subscription" inManagedObjectContext:self.managedObjectContext]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"subscriptionID == %@", self.subscription.subscriptionID]];
    
    NSError *error = nil;
    Subscription *subscriptionObject = [[self.managedObjectContext executeFetchRequest:request error:&error] firstObject];
    
    if(subscriptionObject) {
        NSString *title = [self.titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *source = [self.sourceTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if(title.length > 0) {
            subscriptionObject.title = title;
        }
        
        if(source.length > 0) {
            subscriptionObject.source = source;
            
            NSString *iconURLString = [@"https://www.google.com/s2/favicons?domain=" stringByAppendingString:source];
            subscriptionObject.iconURL = iconURLString;
        }
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        } else {
            [self.delegate editedSubscription:subscriptionObject onIndexPath:self.indexPath];
        }
    }
}

@end
