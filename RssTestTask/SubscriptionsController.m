//
//  SourcesController.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "SubscriptionsController.h"
#import "AddSubscriptionController.h"
#import "DataManager.h"
#import "Subscription+CoreDataProperties.h"
#import "PostListController.h"
#import "EditSubscriptionController.h"

@interface SubscriptionsController () <AddSubscriptionControllerDelegate, EditSubscriptionControllerDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContex;
@property (strong, nonatomic) NSArray *subscriptionsArray;

@end

@implementation SubscriptionsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.managedObjectContex = [[[DataManager instance] persistentContainer] viewContext];
    
    [self getSubscriptions];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)getSubscriptions {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Subscription"];
    
    NSSortDescriptor *sortDateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDateDescriptor]];
    
    NSError *error = nil;
    NSArray *results = [self.managedObjectContex executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"Fetch error: %@", [error localizedDescription]);
    }
    
    self.subscriptionsArray = results;
    
    [self.tableView reloadData];
}

#pragma mark - IBActions

- (IBAction)addSubscriptionAction:(UIBarButtonItem *)sender {
    AddSubscriptionController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddSubscriptionController"];
    vc.delegate = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.subscriptionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"SubscriptionsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    Subscription *subscription = [self.subscriptionsArray objectAtIndex:indexPath.row];
    
    __weak UITableViewCell *weakCell = cell;
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: subscription.iconURL]];

        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *icon = (data != nil) ? [UIImage imageWithData: data] : [UIImage imageNamed:@"rss_nologo.png"];
            
            weakCell.imageView.image = icon;
            [weakCell layoutSubviews];
        });
    });
    
    cell.textLabel.text = subscription.title;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Subscription *subscription = [self.subscriptionsArray objectAtIndex:indexPath.row];
    
    PostListController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PostListController"];
    vc.subscription = subscription;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    Subscription *subscription = [self.subscriptionsArray objectAtIndex:indexPath.row];
    
    UITableViewRowAction *deleteButton = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self.managedObjectContex deleteObject:subscription];
        
        NSError * error = nil;
        if (![self.managedObjectContex save:&error]) {
            NSLog(@"Delete subscription error: %@", [error localizedDescription]);
        }
        
        NSMutableArray *subscriptionsArrayTemp = [NSMutableArray arrayWithArray:self.subscriptionsArray];
        
        [subscriptionsArrayTemp removeObjectAtIndex:indexPath.row];
        
        self.subscriptionsArray = subscriptionsArrayTemp;
        
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }];
    
    UITableViewRowAction *updateButton = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Update" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        EditSubscriptionController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditSubscriptionController"];
        vc.subscription = subscription;
        vc.delegate = self;
        vc.indexPath = indexPath;
        
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    updateButton.backgroundColor = [UIColor colorWithRed: 51.0/255.0 green: 153.0/255.0 blue:255.0/255.0 alpha: 1.0];
    
    return @[deleteButton, updateButton];
}

#pragma mark - AddSubscriptionControllerDelegate

- (void)addedNewSubscription:(Subscription *)subscription {
    [self.navigationController popViewControllerAnimated:YES];
    
    NSMutableArray *subscriptionsArrayTemp = [NSMutableArray arrayWithArray:self.subscriptionsArray];
    [subscriptionsArrayTemp insertObject:subscription atIndex:0];
    self.subscriptionsArray = subscriptionsArrayTemp;
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
}

#pragma mark - EditSubscriptionControllerDelegate

- (void)editedSubscription:(Subscription *)subscription onIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *subscriptionsArrayTemp = [NSMutableArray arrayWithArray:self.subscriptionsArray];
    
    [subscriptionsArrayTemp replaceObjectAtIndex:indexPath.row withObject:subscription];
    
    self.subscriptionsArray = subscriptionsArrayTemp;
    
    [self.tableView reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
