//
//  EditSubscriptionController.h
//  RssTestTask
//
//  Created by alexander.oschepkov on 28.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Subscription+CoreDataProperties.h"

@protocol EditSubscriptionControllerDelegate;

@interface EditSubscriptionController : UITableViewController

@property (weak, nonatomic) id <EditSubscriptionControllerDelegate> delegate;
@property (strong, nonatomic) Subscription *subscription;
@property (strong, nonatomic) NSIndexPath *indexPath;

@end

@protocol EditSubscriptionControllerDelegate

- (void)editedSubscription:(Subscription *)subscription onIndexPath:(NSIndexPath *)indexPath;

@end
