//
//  PostDetailController.h
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"

@interface PostDetailController : UIViewController

@property (strong, nonatomic) PostModel *post;

@end
