//
//  AddSubscriptionController.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "AddSubscriptionController.h"
#import "DataManager.h"
#import "Subscription+CoreDataProperties.h"

@interface AddSubscriptionController ()

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *sourceTextField;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation AddSubscriptionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Добавить подписку";
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveSubscription:)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
    self.managedObjectContext = [[[DataManager instance] persistentContainer] viewContext];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)saveSubscription:(UIBarButtonItem *)sender {
    NSString *titleValue = [self.titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *sourceValue = [self.sourceTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(titleValue.length > 0 && sourceValue.length > 0) {
        Subscription *newObject = [NSEntityDescription insertNewObjectForEntityForName:@"Subscription" inManagedObjectContext:self.managedObjectContext];
        
        newObject.subscriptionID = [self generateUnicId];
        newObject.title = titleValue;
        newObject.source = sourceValue;
        
        NSString *iconURLString = [@"https://www.google.com/s2/favicons?domain=" stringByAppendingString:sourceValue];
        newObject.iconURL = iconURLString;
        
        newObject.createdDate = [NSDate date];
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        
        [self.delegate addedNewSubscription:newObject];
    } else {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please fill in all fields" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (NSString *)generateUnicId {
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

@end
