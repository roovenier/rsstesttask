//
//  PostModel.h
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostModel : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSDate *pubDate;

- (instancetype)initWithData:(NSDictionary *)dataDictionary;

@end
