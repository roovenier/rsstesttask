//
//  AppDelegate.m
//  RssTestTask
//
//  Created by alexander.oschepkov on 24.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "AppDelegate.h"
#import "DataManager.h"
#import "Subscription+CoreDataProperties.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    BOOL isPreloaded = [defaults boolForKey:@"isPreloaded"];
    if(!isPreloaded) {
        [self settingInitialData];
        [defaults setBool:YES forKey:@"isPreloaded"];
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [[DataManager instance] saveContext];
}

#pragma mark - Actions

- (void)settingInitialData {
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"rssdb" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSManagedObjectContext *managedObjectContext = [[[DataManager instance] persistentContainer] viewContext];
    
    for(NSDictionary *item in [jsonDictionary objectForKey:@"items"]) {
        Subscription *subscription = [NSEntityDescription insertNewObjectForEntityForName:@"Subscription" inManagedObjectContext:managedObjectContext];
        subscription.subscriptionID = [item objectForKey:@"subscriptionID"];
        subscription.title = [item objectForKey:@"title"];
        subscription.source = [item objectForKey:@"source"];
        subscription.iconURL = [item objectForKey:@"iconURL"];
        subscription.createdDate = [NSDate dateWithTimeIntervalSince1970:[[item objectForKey:@"createdDate"] doubleValue]];
        
        NSError *error = nil;
        if(![managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}

@end
